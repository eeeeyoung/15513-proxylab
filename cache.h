/**
 * @file cache.h
 * 
 * Definitions for functions involving multithreaded cache operations of a 
 * simple cache design utilized in proxylab as a temporary storage solution,
 * with support for Least Recently Used (LRU) eviction policy. Functions
 * include cache initialization, cache object lookup, cache object insertion
 * and eviction. Cache objects have limits in maximum object size and the 
 * entire cache has a limited cumulative size.
 * 
 * This cache implementation makes use of singly linked list and structs to
 * store cache objects and cache-related info. At creation, cache objects are
 * dynamically allocated on the heap. Cache objects are looked up via their
 * request URL. At each cache read-hit and cache object creation, we increment
 * age of all other cache object's as a way to keep track of the least-recently
 * used cache object.
 * 
 * This cache implementation uses pthread_mutex_t to control read and write
 * accesses to the shared data structure. Threads waits for obtaining the lock
 * to either scan for cache object or insert a new object. The lock is released
 * when insertion completes or cache object is located. Reference counts are 
 * used to keep track of the number of reading threads currently writing to
 * clients in response in order to avoid premature eviction. Incrementing and
 * decrementing reference counts are protected by another separate mutex.
 */

#include <pthread.h>
#include <stdio.h>
#include <stdbool.h>

#define MAX_CACHE_SIZE (1024*1024)
#define MAX_OBJECT_SIZE (100*1024)

/* Struct for storing cache object and all related information */
typedef struct Cache_obj{
    int obj_size;
    int ref_count;
    int age;
    struct Cache_obj *next;
    char *URL;
    char *objectfile;
} Cache_obj_t;

/* 
 * Cache head with information on remaining capacity and pointer to the first
 * cache object in the linked list.
 */
typedef struct {
    int avail_capacity;
    Cache_obj_t *next;
} Cache_t;

/* 
 * #######################
 * Cache related functions
 * ####################### 
 */

/**
 * cache_init: Initializes the cache data structure:
 *             1) Allocates the cache head on the heap and sets init capacity.
 *             2) Initializes pthread_mutex for read/write and refcounts.
 * 
 * @Error:     1) memory allocations may fail.
 *             2) pthread_mutex_init may fail.
 * 
 * @Precondition: cache_init is called once and no lock is required.
 */ 
void cache_init();

/**
 * scan_cache: Scans the linked list for cache object with URL identical to
 *             the input string key. If target object is found, sets its age
 *             to zero and return a pointer to the object; otherwise, returns
 *             NULL.
 * 
 * key:       URL key to look for in the cache linked list.
 * 
 * Preconditions: pthread_mutex_lock mutex must be obtained by the calling
 *                function.
 */
Cache_obj_t* scan_cache(char *key);

/**
 * evict_least_used: Traverses through the cache linked list to find the cache
 *                   object with the eldest age. The eviction is performed by
 *                   memset to clear URL key and cache objectfile and resetting
 *                   age, filesize to 0. Leaves pointer to next cache object
 *                   unchanged to maintain the linked list data structure. Adds
 *                   the evicted object's size to cache head's capacity.
 * 
 *                   Eviction must wait until all reading threads finishes
 *                   writing the buffer to their clients. If any such reading 
 *                   threads exist, the refcnt is > 0. Uses a spinning while
 *                   loop to keep monitoring the refcnt value.
 * 
 * Preconditions: pthread_mutex_lock mutex already obtained by calling function.
 *                refcnt mutex may still be holding by other threads.
 */
void evict_least_used();

/**
 * insert:   Inserts cache object into the linked list. The place to insert
 *           is either at the end of the linked list or any other evicted spots
 *           that are in the middle of the linked list. One or more eviction 
 *           may be required if remaining capacity is insufficient.
 * 
 * key:      URL to be used as key. Maxsize defined in macro.
 * buf:      char object buffer to be used as value. Maxsize defined in macro.
 * filesize: the size of the buffer object.
 * 
 * Preconditions: pthread_mutex_lock mutex already obtained by calling function
 */
void insert(char *key, char *buf, int filesize);

/*
 * ########################
 * List of Helper functions
 * ########################
 */
/**
 * increment_age_except: Helper function for incrementing age of cache object
 *                       by 1 except the one being used or inserted.
 * 
 * key:                  URL key to not increase in the cache linked list.
 * 
 * Preconditions: pthread_mutex_lock mutex already obtained by calling function
 */
void increment_age_except(char *key);

/**
 * is_duplicate: Check for identical key in the cache. In some circumstances,
 *               cache object to be inserted might already exists in the linked
 *               list due to the multithreaded nature of the cache.
 * 
 * Preconditions: pthread_mutex_lock mutex already obtained by calling function
 */
bool is_duplicate(char *key);