/*
 * Name: Yang YI
 * Andrew ID: yangyi
 * 
 * @file: proxy.c - Multi-thread web proxy with cache.
 * 
 * Implementation details:
 * 1. use multi-thread to allow concurrency
 * 2. maintain global variable(cache list and header) update thread safe
 *    using pthread mutex.
 * 3. parse client request - form cache id - search for cache
 *    if cache hit - form response and return directly
 *    if cache miss - request from server and update cache
 */

/* Some useful includes to help you get started */

#include "csapp.h"
#include "cache.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <assert.h>

#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netdb.h>

#define HOSTLEN 256
#define SERVLEN 8

/*
 * Debug macros, which can be enabled by adding -DDEBUG in the Makefile
 * Use these if you find them useful, or delete them if not
 */
#ifdef DEBUG
#define dbg_assert(...) assert(__VA_ARGS__)
#define dbg_printf(...) fprintf(stderr, __VA_ARGS__)
#else
#define dbg_assert(...)
#define dbg_printf(...)
#endif

/*
 * Max cache and object sizes
 * You might want to move these to the file containing your cache implementation
 */
#define MAX_CACHE_SIZE (1024*1024)
#define MAX_OBJECT_SIZE (100*1024)
#define MAX_RESPONSE_SIZE (MAX_OBJECT_SIZE*20)
#define MAX_PORT_LEN 10
#define MAX_HOST_LEN 255
#define HTTP_FORMAT_LENGTH 7

/*
 * String constants to use as User-Agent header, connection headers and
 * proxy connection headers in HTTP request headrers.
 */
static const char *header_user_agent = "User-Agent: Mozilla/5.0"
                                    " (X11; Linux x86_64; rv:3.10.0)"
                                    " Gecko/20190801 Firefox/63.0.1\r\n";
static const char *connection_hdr = "Connection: close\r\n";
static const char *proxy_connection_hdr = "Proxy-Connection: close\r\n";

/* Typedef for convenience */
typedef struct sockaddr SA;

/* Information about a connected client. */
typedef struct {
    struct sockaddr_in addr;    // Socket address
    socklen_t addrlen;          // Socket address length
    int connfd;                 // Client connection file descriptor
    char host[HOSTLEN];         // Client host
    char serv[SERVLEN];         // Client service (port)
} client_info;

/* URI parsing results. */
typedef enum {
    PARSE_ERROR,
    PARSE_STATIC,
    PARSE_DYNAMIC
} parse_result;


/* List of functions */
void sigpipe_handler(int sig);
void *thread(void *vargp);
void serve(client_info *client);
void clienterror(int fd, char *cause, char *errnum,
        char *shortmsg, char *longmsg);
int parse_uri(char *uri, char *host, char *port, char *path);
int append_headers(char *buf, char *headers, char*port_num);
int send_request(char *host, char *port, char *reqline, char *header);
int get_client_info(client_info *client);

/* read/write related functions */
int read_from_cache(char *key, int fd);
void write_to_cache(char *key, char *obj, int filesize);

extern Cache_t *Cache;
/* pthread mutex for controlling read/write access*/
pthread_mutex_t mutex;
/* pthread mutex for controlling refcnt */
pthread_mutex_t refcnt_mutex;

/*
 * main:  main entrance for the proxy server. A while loop is used
 *        to perform the routine of 
 * 
 * argc:  number of cmdline arguments
 * argv:  List of cmdline arguments
 *        1) Listening port number for the web proxy 
 *
 */
int main(int argc, char** argv) {
    int listenfd;
    pthread_t tid;

    // Install the SIGPIPE handler
    Signal(SIGPIPE,  sigpipe_handler);
    cache_init();

    // checking if input is legal
    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }
    if ((listenfd = open_listenfd(argv[1])) < 0) {
        fprintf(stderr, "Failed to listen on port: %s\n", argv[1]);
        exit(1);
    }

    while(1) {
        /* Allocate space on the heap for client info, to be freed in child */
        client_info *client = Malloc(sizeof(client_info));

        /* Initialize the length of the address */
        client->addrlen = sizeof(client->addr);
        
        /* accept() will block until a client connects to the port */
        client->connfd = accept(listenfd,
                (SA *) &client->addr, &client->addrlen);
        if (client->connfd < 0) {
            perror("accept");
            continue;
        }
        pthread_create(&tid, NULL, thread, client);
    }
    return 0;
}

/* thread routine for handling  */
void *thread(void *vargp) {
    Signal(SIGPIPE,  sigpipe_handler);
    client_info *client = (client_info *)vargp;
    if (pthread_detach(pthread_self()))
    {
        printf("pthread_detach error\n");
    }
    serve(client);
    if (close(client->connfd))
    {
        printf("Closing fd error\n");
    }
    free(client);
    return NULL;
}

/*
 * serve - handle one HTTP request/response transaction
 * 
 * Source: tiny.c
 */
void serve(client_info *client) {
    char buf[MAXLINE], uri[MAXLINE], object_buf[MAX_RESPONSE_SIZE],
         req_host[MAX_HOST_LEN], req_port[MAX_PORT_LEN], req_path[MAXLINE],
         request_line[MAXLINE], forward_req_header[MAXLINE * 20];
    int total_size, server_fd;
    rio_t rio, rio_server;
    memset(forward_req_header, 0, sizeof(forward_req_header));
    memset(request_line, 0, sizeof(request_line));

    // Get info about the client (hostname/port)
    if (get_client_info(client) < 0) return;

    /* Read request line, parse the request line and check if well-formed */
    char method[MAXLINE];
    char version;
    rio_readinitb(&rio, client->connfd);
    if (rio_readlineb(&rio, buf, MAXLINE) <= 0) {
        return;
    }

    /* sscanf must parse exactly 3 things for request line to be well-formed */
    /* version must be either HTTP/1.0 or HTTP/1.1 */
    if (sscanf(buf, "%s %s HTTP/1.%c", method, uri, &version) != 3
            || (version != '0' && version != '1')) {
        clienterror(client->connfd, buf, "400", "Bad Request",
                "Proxy received a malformed request");
        return;
    }

    /* Check that the method is GET */
    if (strncmp(method, "GET", sizeof("GET"))) {
        clienterror(client->connfd, method, "501", "Not Implemented",
                "Proxy does not implement this method");
        return;
    }

    printf("request uri is %s\n", uri);

    // Attempt to read from cache
    if (read_from_cache(uri, client->connfd) > 0) return;

    /* Code for parsing URI */
    if ((parse_uri(uri, req_host, req_port, req_path) < 0)) {
        sio_printf("Invalid uri!\n");
        return;
    } else {
        snprintf(request_line, sizeof(request_line), "%s%s%s",
                 "GET ", req_path, " HTTP/1.0\r\n");
    }
    snprintf(forward_req_header, sizeof(forward_req_header), "%s%s%s",
             connection_hdr, proxy_connection_hdr, header_user_agent);

    // Source: code/netp/tiny/tiny.c
    rio_readlineb(&rio, buf, MAXLINE);
    while(strcmp(buf, "\r\n")) {
        if (append_headers(buf, forward_req_header, req_port) < 0) {
            sio_printf("Parse Request Header Error\n");
            return;
        }
        rio_readlineb(&rio, buf, MAXLINE);
    }

    // if Host header is absent in request headers
    if (strstr(forward_req_header, "Host: ") == NULL) {
        char header_host[MAXLINE];
        memset(header_host, 0, sizeof(header_host));
        snprintf(header_host, sizeof(header_host), "%s%s%s%s%s",
                 "Host: ", req_host, ":", req_port, "\r\n");
        strcat(forward_req_header, header_host);
    }
    strcat(forward_req_header, "\r\n");

    server_fd = send_request(req_host, req_port, request_line, 
                          forward_req_header);
    rio_readinitb(&rio_server, server_fd);
    total_size = rio_readnb(&rio_server, object_buf, sizeof(object_buf));
    if (total_size <= MAX_OBJECT_SIZE && total_size > 0) {
        write_to_cache(uri, object_buf, total_size);
    }
    rio_writen(client->connfd, object_buf, total_size);
    
    // prematurely closing file descriptor may interrupt file transmission
    // A makeshift solution is to sleep for a certain period before closing
    close(server_fd);
}

/* Forwarding the request to the server */
int send_request(char *host, char *port, char *reqline, char *header)
{
    int fd;
    char buf[MAXLINE], *head = buf;
    rio_t rio;

    if ((fd = open_clientfd(host, port)) < 0)
    {
        printf("open_clientfd error\n");
    }
    rio_readinitb(&rio, fd);
    sprintf(head, reqline);
    head = buf + strlen(buf);
    sprintf(head, header);
    rio_writen(fd, buf, MAXLINE);
    return fd;
}

/*
 * ################
 * Helper Functions
 * ################
 */

/**
 * Dummy handler that catches SIGPIPE and prevent proxy from terminating. 
 */
void sigpipe_handler(int sig)
{
    sio_printf("SIGPIPE signal caught\n");
    return;
}

/* Source: CS:APP textbook */
int get_client_info(client_info *client)
{
    int res = getnameinfo(
            (SA *) &client->addr, client->addrlen,
            client->host, sizeof(client->host),
            client->serv, sizeof(client->serv),
            0);
    if (res == 0) {
        printf("Accepted connection from %s:%s\n", client->host, client->serv);
        return res;
    }
    else {
        fprintf(stderr, "getnameinfo failed: %s\n", gai_strerror(res));
        return -1;
    }
}

/* Helper function for  parsing the uri and storing into different char arrays*/
int parse_uri(char *uri, char *host, char *port, char *path)
{
    char *temp;
    if (strstr(uri, "http://") != uri) {
        sio_printf("Encountered invalid uri!\n");
        return -1;
    }
    uri += HTTP_FORMAT_LENGTH;  // strlen of "http://"

    // no specified port from client
    if ((temp = strstr(uri, ":")) == NULL) {
        strcpy(port, "80");
        temp = strstr(uri, "/");
        *temp = '\0';
        strcpy(host, uri);
    } else {
        *temp = '\0';
        strcpy(host, uri);
        *temp = ':';
        uri = temp + 1;
        temp = strstr(uri, "/");
        *temp = '\0';
        strcpy(port, uri);
    }
    *temp = '/';
    strcpy(path, temp);
    return 0;
}

/* Helper function for constructing the forward request's headers */
int append_headers(char *buf, char *headers, char*port_num)
{
    char *temp = strstr(buf, ": ");
    int is_host = 0;
    *temp = '\0';
    if (!strcmp(buf, "User-Agent")) return 1;
    if (!strcmp(buf, "Connection")) return 1;
    if (!strcmp(buf, "Proxy-Connection")) return 1;
    if (!strcmp(buf, "Host")) {
        is_host = 1;
    }
    *temp = ':';
    strcat(headers, buf);
    temp += 1;
    if (strstr(temp, ":") == NULL && is_host == 1) {
        temp = strstr(headers, "\r\n");
        strcpy(temp, port_num);
        temp += strlen(port_num);
        strcpy(temp, "\r\n");
    }
    return 0;
}

/* Helper function for invoking */
int read_from_cache(char *key, int fd)
{
    Cache_obj_t* obj;

    pthread_mutex_lock(&mutex);
    if ((obj = scan_cache(key)) == NULL) {
        pthread_mutex_unlock(&mutex);
        return -1;  // Not found
    } else {
        pthread_mutex_lock(&refcnt_mutex);
        obj->ref_count++;
        pthread_mutex_unlock(&refcnt_mutex);

        pthread_mutex_unlock(&mutex);  // unlocking then send to client
        rio_writen(fd, obj->objectfile, obj->obj_size);
        
        pthread_mutex_lock(&refcnt_mutex);  // when finished transfering
        obj->ref_count--;
        pthread_mutex_unlock(&refcnt_mutex);
        close(fd);
        return 1;
    }
}

/* Wrapper function for invoking write to cache function.
 * Obtains and releases lock.
 */
void write_to_cache(char *key, char *obj, int filesize)
{
    pthread_mutex_lock(&mutex);
    insert(key, obj, filesize);
    pthread_mutex_unlock(&mutex);
}

/*
 * clienterror - returns an error message to the client
 */
void clienterror(int fd, char *cause, char *errnum,
        char *shortmsg, char *longmsg) {
    char buf[MAXLINE];
    char body[MAXBUF];
    size_t buflen;
    size_t bodylen;

    /* Build the HTTP response body */
    bodylen = snprintf(body, MAXBUF,
            "<!DOCTYPE html>\r\n" \
            "<html>\r\n" \
            "<head><title>Tiny Error</title></head>\r\n" \
            "<body bgcolor=\"ffffff\">\r\n" \
            "<h1>%s: %s</h1>\r\n" \
            "<p>%s: %s</p>\r\n" \
            "<hr /><em>The Tiny Web server</em>\r\n" \
            "</body></html>\r\n", \
            errnum, shortmsg, longmsg, cause);
    if (bodylen >= MAXBUF) {
        return; // Overflow!
    }

    /* Build the HTTP response headers */
    buflen = snprintf(buf, MAXLINE,
            "HTTP/1.0 %s %s\r\n" \
            "Content-Type: text/html\r\n" \
            "Content-Length: %zu\r\n\r\n", \
            errnum, shortmsg, bodylen);
    if (buflen >= MAXLINE) {
        return; // Overflow!
    }

    /* Write the headers */
    if (rio_writen(fd, buf, buflen) < 0) {
        fprintf(stderr, "Error writing error response headers to client\n");
        return;
    }

    /* Write the body */
    if (rio_writen(fd, body, bodylen) < 0) {
        fprintf(stderr, "Error writing error response body to client\n");
        return;
    }
}