/**
 * @file: cache.c
 * @brief Function implementations for multithreaded cache declared in cache.h
 *        with support for LRU eviction policy.
 */

#include "cache.h"
#include "csapp.h"

#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

Cache_t * Cache;
extern pthread_mutex_t mutex;
extern pthread_mutex_t refcnt_mutex;

/* Initializes cache, mutex and initial capacity. */
void cache_init()
{
    if (pthread_mutex_init(&mutex, NULL) != 0 ||
        pthread_mutex_init(&refcnt_mutex, NULL) != 0) {
            printf("\n mutex init failed\n");
            exit(1);
        }
    Cache = (Cache_t *)Malloc(sizeof(Cache_t));
    Cache->avail_capacity = MAX_CACHE_SIZE;
    Cache->next = NULL;
}

/* Scans the cache for cache object having URL as key */
Cache_obj_t* scan_cache(char *key)
{
    Cache_obj_t *next = Cache->next;
    while (next != NULL) {
        // if URL key matches and object size not zero.
        if ((!strcmp(key, next->URL)) && next->obj_size != 0) {
            increment_age_except(key);
            next->age = 0;
            return next;
        }
        next = next->next;  // move on to the next cache object
    }
    return NULL;
}

/* Inserts cache object into the linkedlist by dynamically allocating */
void insert(char *key, char *buf, int filesize)
{
    Cache_obj_t *head = Cache->next;
    Cache_obj_t *next;
    if (is_duplicate(key)) return;
    if (Cache->avail_capacity >= filesize) {
        Cache->avail_capacity -= filesize;
        if (head == NULL) {
            // init, can do better if given time!
            next = Malloc(sizeof(Cache_obj_t));
            next->objectfile = Malloc(MAX_OBJECT_SIZE + 1);
            memset(next->objectfile, 0, MAX_OBJECT_SIZE + 1);
            next->URL = Malloc(MAXLINE + 1);
            memset(next->URL, 0, MAXLINE + 1);

            next->age = 0;
            next->obj_size = filesize;
            next->next = NULL;
            next->ref_count = 0;
            memcpy((void *)next->objectfile, (void *)buf, filesize);
            strcpy(next->URL, key);
            Cache->next = next;
        } else {
            next = head->next;
            while (next != NULL && next->obj_size != 0) {
                head = next;
                next = next->next;
            }
            if (next == NULL) {
                next = Malloc(sizeof(Cache_obj_t));
                next->objectfile = Malloc(MAX_OBJECT_SIZE + 1);
                next->URL = Malloc(MAXLINE + 1);
                next->next = NULL;
            }
            memset(next->objectfile, 0, MAX_OBJECT_SIZE + 1);
            memset(next->URL, 0, MAXLINE + 1);
            next->age = 0;
            next->obj_size = filesize;
            next->ref_count = 0;
            memcpy((void *)next->objectfile, (void *)buf, filesize);
            strcpy(next->URL, key);
            head->next = next;
        }
        printf("inserted %s\n", next->URL);
        increment_age_except(key);
    } else {
        while (Cache->avail_capacity < filesize) {
            evict_least_used();
        }
        insert(key, buf, filesize);
    }
}

/* Evicts the least-used cache object (with the eldest age). */
void evict_least_used()
{
    int max_age = -1;
    Cache_obj_t *least = NULL;
    Cache_obj_t *next = Cache->next;
    while (next != NULL) {
        if (next->age > max_age && next->obj_size > 0) {
            max_age = next->age;
            least = next;
        }
        next = next->next;
    }
    // wait for refcnt lock
    while (least->ref_count > 0) {
        sleep(1);
    }
    least->age = 0;
    Cache->avail_capacity += least->obj_size;
    least->obj_size = 0;
    memset(least->URL, 0, MAXLINE+1);
    memset(least->objectfile, 0, MAX_OBJECT_SIZE+1);
}

/* Helper function for incrementing  */
void increment_age_except(char *key)
{
    Cache_obj_t *next = Cache->next;
    while (next != NULL) {
        if (strcmp(key, next->URL) && next->obj_size > 0) {
            next->age++;
        }
        next = next->next;
    }
}

/* Helper function for checking for identical key in the cache */
bool is_duplicate(char *key)
{
    Cache_obj_t *head = Cache->next;
    while (head != NULL) {
        if (!strcmp(key, head->URL)) return true;
        head = head->next;
    }
    return false;
}